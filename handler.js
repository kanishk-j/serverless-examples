// const puppeteer = require('puppeteer');
// const { getChrome } = require('./chrome-script');
const test = require('./test');
const chromium = require('chrome-aws-lambda');

module.exports.test = async (event) => {
  console.log(event.queryStringParameters);
  const token = event.queryStringParameters;
  // const chrome = await getChrome();
  // const browser = await puppeteer.connect({
  //   browserWSEndpoint: chrome.endpoint,
  // });

  try {
    browser = await chromium.puppeteer.launch({
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      executablePath: await chromium.executablePath,
      headless: chromium.headless,
      ignoreHTTPSErrors: true,
    });

    await test.loadSingleChat(browser, token);
  } catch(err) {
    console.log(err);
  }
};
