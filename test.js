const puppeteer = require('puppeteer');
// const chat_tokens = require('./tokens');
const fs = require('fs');
const yes_lets_begin = 'div.mui-container-fluid.null:last-child div.mui-panel.message-container.left-container.null > .message-item button';
const last_mood_emoji = 'div.mui-container-fluid.null:last-child div.mui-panel.message-container.left-container.null > .message-item span.options:last-child img';
const first_mcq = 'div.mui-container-fluid.null:last-child div.mui-panel.message-container.left-container.null:last-child > div.message-item:last-child span.options:first-child > button';
const done = 'div.mui-container-fluid.null:last-child div.mui-panel.message-container.left-container.null:last-child > div.message-item:last-child span:first-child > button';

// console.log(chat_tokens.length, chat_tokens[0])

async function loadSingleChat (browser, token) {
    try {
        console.time();
        console.log('browser started');
        await openBrowserPage(browser, token);
        console.log('done');
        console.timeEnd();
    } catch (err) {
        console.log(err);
    }
}

async function load () {
    console.time();
    console.log('browser started');
    let _chat_tokens = [];
    try {
        for(let i in chat_tokens) {
            _chat_tokens.push(chat_tokens[i]);
            if(_chat_tokens.length == 500) {
                await Promise.all(
                    openBrowserChats(_chat_tokens)
                );

                _chat_tokens = [];
            }
        } 
        console.log('done');
        console.timeEnd();
        // browser.close();
    } catch(err) {
        console.log(err);
        // browser.close();
    }
}

function openBrowserChats (chat_tokens) {
    let promises = [];
    for(let i=0; i<chat_tokens.length; i+=10) {
        let _chat_tokens = chat_tokens.slice(i, i+10);
        promises.push(
            openBrowser(_chat_tokens)
        );
    }

    return promises;
}

async function openBrowser(browser, chat_tokens) {
    // let browser = await puppeteer.launch(
    //     {
    //         executablePath: '/usr/bin/google-chrome',
    //         args: ['--enable-precise-memory-info','--max_old_space_size = 185000']
    //         // headless: false
    //     });
    try {
        await Promise.all(
            chat_tokens.map(user => openBrowserPage(browser, user))
        );
        browser.close();
    } catch(err) {
        console.log(err);
        browser.close();
    }
}

async function openBrowserPage(browser, token) {
    try {
        let user = {
            email: token.email,
            performance: []
        }
        
        let t1=0,t2=0;

        const page = await browser.newPage();
        console.log('page opened for ', token.email);
        console.log(`http://chat.dev.amber.infeedo.com/#/token:${token.token}`);
        await page.goto(`http://chat.dev.amber.infeedo.com/#/token:${token.token}`);
        await page.setRequestInterception(true);

        page.on('request', (req) => {
            if(req.resourceType() === 'image' || req.resourceType() == 'stylesheet' || req.resourceType() == 'font'){
                req.abort();
            }
            else {
                req.continue();
            }
        });

        // await page.waitForSelector(yes_lets_begin, {
        //     timeout: 100000
        // })
        // t1 = Date.now();
        // await page.click(yes_lets_begin);
        // await page.waitForSelector(last_mood_emoji, {
        //     timeout: 100000
        // })
        // t2 = Date.now();

        // user.performance.push((t2-t1) / 1000);

        // t1 = Date.now();
        // await page.click(last_mood_emoji);
        // await page.waitForSelector(first_mcq, {
        //     timeout: 100000
        // });
        // t2 = Date.now();

        // user.performance.push((t2-t1) / 1000)
        // for (let i = 0; i < 5; i++) {
        //     t1 = Date.now();
        //     await page.click(first_mcq);
        //     await page.waitFor(10000)
        //     t2 = Date.now();

        //     user.performance.push((t2-t1) / 1000);
        // }
        // await page.waitForSelector(done, {
        //     timeout: 100000
        // })
        // await page.click(done);

        await page.waitForSelector(yes_lets_begin, {
            timeout: 100000
        })
        t1 = Date.now();
        await page.click(yes_lets_begin);
        await page.waitForSelector(last_mood_emoji, {
            timeout: 100000
        })
        t2 = Date.now();

        user.performance.push((t2-t1) / 1000);

        let num = await page.evaluate(
            () => document.querySelectorAll('div.mui-container-fluid.null').length
        )

        console.log(num);

        t1 = Date.now();
        await page.click(last_mood_emoji);
        await page.waitForSelector(`div.mui-container-fluid.null:nth-of-type(${num+3}) div.mui-panel.message-container.left-container.null:last-child > div.message-item:last-child span.options:first-child > button`, {
            timeout: 100000
        });
        t2 = Date.now();

        user.performance.push((t2-t1) / 1000)
        num += 3;
        for (let i = 0; i < 5; i++) {
            console.log(num);
            t1 = Date.now();
            await page.click(first_mcq);
            if(i != 4)
                await page.waitForSelector(`div.mui-container-fluid.null:nth-of-type(${num+3}) div.mui-panel.message-container.left-container.null:last-child > div.message-item:last-child span.options:first-child > button`, {
                    timeout: 100000
                })
            t2 = Date.now();

            user.performance.push((t2-t1) / 1000);
            num += 3;
        }
        await page.waitForSelector(done, {
            timeout: 100000
        })
        await page.evaluate(() => {
            document.querySelector('div.mui-container-fluid.null:last-child div.mui-panel.message-container.left-container.null:last-child > div.message-item:last-child span:first-child > button').click();
        });

        await page.waitFor(2000);
        // await page.waitForNavigation()
        // await page.screenshot({ path: 'chat.png' });
        await page.close();
        // fs.appendFileSync('./performance.log', `${JSON.stringify(user)}\n`);
        await browser.close();
    } catch(err){
        console.log('error with chat for ', token.email, err);
        await browser.close();
    }
}

module.exports = {
    load,
    loadSingleChat
}

// load();
// loadSingleChat(chat_tokens[101]);